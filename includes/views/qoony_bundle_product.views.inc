<?php

/**
 * @file
 * Some views addition for qoony_bundle_product module.
 */

/**
 * Implements hook_views_data_alter().
 */
function qoony_bundle_product_views_data_alter(&$data) {
  // Add an custom field handler for product title field, that
  // the bundle products appears a readble name.
  $data['commerce_product']['bundle_product_line_item_title'] = array(
    'real field' => 'title',
    'title' => t('Bundle product readable line item title'),
    'help' => t('Provide an readable bundle product title'),
    'field' => array(
      'handler' => 'qoony_bundle_product_readable_title',
    ),
  );
}
