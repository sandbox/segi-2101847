<?php

/**
 * @file
 * Define a custom views handler.
 */

/**
 * If a line item is bundle product, then change the title for readable format.
 */
class qoony_bundle_product_readable_title extends views_handler_field {

  function render($values) {
    // Get the title value.
    $title = $values->{$this->field_alias};

    // Try to decide it is bundle product or not.
    $product = commerce_product_load_by_sku($title);

    if (!empty($product)) {
      $bundle_product_list = qoony_bundle_product_ordered_product_list($product);
      if (isset($bundle_product_list['bundle_products'])) {
        $new_title = '';
        foreach ($bundle_product_list['bundle_products'] as $delta => $product) {
          // Create bundle product custom title.
          if (empty($new_title)) {
            $new_title .= $product['title'];
          }
          else {
            $new_title .= ' + ' . $product['title'];
          }
        }
        return $new_title;
      }
    }
    return $title;
  }
}