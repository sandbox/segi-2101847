<?php
/**
 * @file
 * Template to display products of a bundle.
 */
?>
<div class="header">
  <div class="discount">
    <?php if($discount_percent): ?>
      <div class="percentage">
        <?php print $discount_percent . '%'; ?>
      </div>
    <?php endif; ?>
    <div><?php print t('Bundle discount'); ?></div>
  </div>
  <?php if ($delivery_time): ?>
    <div class='delivery'>
      <?php print t('Current status !status', array('!status' => $delivery_time)); ?>
    </div>
  <?php endif; ?>
</div>
<div class='bundle-product-row clearfix'>
  <div class='products-list'>
    <?php foreach ($bundle_image_list as $delta => $product): ?>
      <?php if (!$delta): ?>
        <?php if ($product['product']['image']): ?>
          <div class='product-item product-item-first'>
            <?php print $product['product']['image']; ?>
            <?php if ($product['product']['title']): ?>
              <p class='title'><?php print $product['product']['title']; ?></p>
            <?php endif; ?>
           </div>
        <?php endif; ?>
      <?php elseif (end($bundle_image_list) == $delta): ?>
        <div class='product-item'>
          <?php if ($product['product']['image']): ?>
            <?php print $product['product']['image']; ?>
            <?php if ($product['product']['title']): ?>
              <p class='title'><?php print $product['product']['title']; ?></p>
            <?php endif; ?>
          <?php endif; ?>
        </div>
      <?php else: ?>
        <?php if ($product['product']['image']): ?>
          <div class='product-item product-item-last'>
            <?php print $product['product']['image']; ?>
            <?php if ($product['product']['title']): ?>
              <p class='title'><?php print $product['product']['title']; ?></p>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      <?php endif; ?>
      <?php if (!empty($product['free_product']['image'])): ?>
        <div class='free-product-item free-product-item'>
          <?php print $product['free_product']['image'] ?>
          <p class='title'>
            <span class='label'><?php print t('Free') . ':' ?></span>
            <?php print $product['free_product']['title']; ?>
          </p>
        </div>
      <?php endif; ?>
    <?php endforeach; ?>
  </div>
  <div class='bundle-addcart'>
    <div class='bundle-prices'>
      <?php if ($price_total): ?>
        <span class='full-price'><?php print t('From') . ' ' . $price_total ?></span>
      <?php endif; ?>
      <?php if ($price): ?>
       <span class='discount-price'><?php print t('Now') . ' ' .  $price; ?></span>
      <?php endif; ?>
    </div>
    <?php if ($add_to_card_form): ?>
      <?php print $add_to_card_form; ?>
    <?php endif; ?>
  </div>
</div>
