<?php
/**
 * @file
 * Render the full list of bundles on parent product page.
 */
?>
<?php if ($anchor_menu): ?>
  <?php print $anchor_menu ?>
<?php endif; ?>
<h2 class="field-label"><a name="product-bundle" id="product-bundle"><?php print t('Bundle actions'); ?></a></h2>
<div class="header">
  <?php foreach ($bundle_products as $bundle_product): ?>
    <?php print drupal_render($bundle_product); ?>
  <?php endforeach; ?>
</div>
