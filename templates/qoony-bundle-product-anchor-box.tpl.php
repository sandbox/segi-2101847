<?php
/**
 * @file
 * Render the anchor box on product page.
 */
?>
<div class='add-cart-bundle'>
  <?php if ($content): ?>
    <?php print drupal_render($content); ?>
  <?php endif; ?>
</div>
