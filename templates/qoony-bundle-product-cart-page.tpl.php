<?php
/**
 * @file
 * Render the bundle list on cart page.
 */
?>
<?php foreach ($product_data as $product): ?>
  <?php if ($product['image_data']['product']): ?>
    <tr>
      <td colspan="6" class="views-field-field-bundle-product">
        <div class="image">
          <?php if ($product['image_data']['product']['image']): ?>
            <?php print $product['image_data']['product']['image'];  ?>
          <?php endif;  ?>
        </div>
        <div class="title">
          <?php if ($product['title']): ?>
            <?php print $product['title'];  ?>
          <?php endif;  ?>
        </div>
      </td>
    </tr>
  <?php endif; ?>
  <?php if ($product['image_data']['free_product']): ?>
    <tr>
      <td colspan="6" class="bundle-free-product">
        <?php if ($product['image_data']['free_product']['image']): ?>
          <div class="image">
            <?php print $product['image_data']['free_product']['image']; ?>
          </div>
        <?php endif;  ?>
        <?php if ($product['image_data']['free_product']['title']): ?>
          <div class="title">
            <span class="orange"><?php print t('Gratuitous')?> </span>
            <?php print $product['image_data']['free_product']['title'];  ?>
            <?php if ($product['free_product']['product_price']): ?>
            <span>
              <?php print '(' . t('t.w.v.') . ' ' .  $product['free_product']['product_price'] . ')'; ?>
            </span>
            <?php endif; ?>
          </div>
        <?php endif;  ?>
      </td>
    </tr>
  <?php endif; ?>
<?php endforeach; ?>