<?php
/**
 * @file
 * qoony_bundle_product.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function qoony_bundle_product_commerce_product_default_types() {
  $items = array(
    'product_bundle' => array(
      'type' => 'product_bundle',
      'name' => 'Product bundle',
      'description' => 'Bundle product type.',
      'help' => '',
      'revision' => 1,
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function qoony_bundle_product_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function qoony_bundle_product_views_api() {
  return array(
    "api" => '3.0',
    "path" => drupal_get_path('module', 'qoony_bundle_product') . '/includes/views',
  );
}
